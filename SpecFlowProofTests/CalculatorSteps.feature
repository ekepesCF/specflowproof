﻿Feature: Calculator
    In order to avoid silly mistakes
    As a math idiot
    I want to be told the sum of two numbers

Scenario Outline: Add two numbers
	Given I have entered <a> into the calculator
    And I have also entered <b> into the calculator
    When I press add
    Then the result should be <sum> on the screen

Examples:
	| a   | b   | sum  |
	| 50  | 70  | 120  |
	| 50  | -70 | -20  |
	| -50 | -70 | -120 |
